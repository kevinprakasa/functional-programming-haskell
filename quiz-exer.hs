import Data.List

fpb x 0 = x
fpb x y = fpb y (x `mod` y)
-- --- fpb 6 3 = fpb 3 (6 mod 3) = fpb 3 (0) = 3
-- -- fpb 5 4 = fpb 4 (5 mod 4) = fpb 4 1 = fpb 1 (4 mod 1) = 1

kpk x y = head [z | z <- [x,x*2 ..], z `mod` y == 0 ]
quicksort [] = []
quicksort (x:xs) = quicksort [y | y <- xs , y<= x] ++ [x] ++ quicksort [z | z <-xs , z > x]

-- maxList arr = foldl max 0 arr

merge [] kanan = kanan
merge kiri [] = kiri
merge kiri@(x:xs) kanan@(y:ys) | x<y = x : merge xs kanan
                               | y <= x = y : merge kiri ys

mergesort xs = merge (mergesort kiri) (mergesort kanan)
  where kiri = take ((length xs )`div`2) xs
        kanan = drop ((length xs)`div`2) xs
-- 1. [ x+1 | x <- xs ]
-- plusOne = map (+1)
-- 2. [ x+y | x <- xs, y <-ys ]
-- func1 x y = zipWith (+) x y
-- plusXY = map
-- 3. [ x+2 | x <- xs, x > 3 ]

-- 4. [ x+3 | (x,_) <- xys ]
-- 5. [ x+4 | (x,y) <- xys, x+y < 5 ]
-- 6. [ x+5 | Just x <- mxs ]

-- maximum (x:xs)
--     | (maximum xs) > x = maximum xs
--     | otherwise        = x

-- permutation
-- perm [] = [[]]
-- perm ls = [ x:ps | x <- ls, ps <- perm(ls\\[x])]

-- sumList :: [Int] -> Int
sumList [] = 0
sumList (x:xs) = x + sumList xs

maxTiga a b c = max a (max b c)

quickSort [] = []
quickSort (x:xs) = quickSort [y | y <- xs, y <= x] ++ x ++ quickSort [y | y <- xs, y > x]



perm [] = [[]]
perm l = [x:y | x <- l, y <- perm (l \\ [x])]

primes = sieve [2..]
  where sieve (x:xs) = x : sieve [y | y<- xs, y `mod` x /= 0]

pythaTriple = [(x,y,z) | z <- [5..]
                        ,y <- [z, z-1 .. 1]
                        ,x <- [y,y-1 .. 1]
                        , z*z == x*x + y*y]


pythaTriple2 = [(x,y,z) | z <- [5..],
                          y <- [1..],
                          x <- [1..y],
                          z*z == x*x + y*y]
pythaTriple3 = [(x,y,z) | y <- [1..],
                          x <- [1..y],
                          z <- [5..],
                          z*z == x*x + y*y]

-- concat [] = []
-- concat (xs: xss) = xs ++ concat xss

-- misteri xs ys = concat [ map (\x -> map(\y -> (x,y)) ys) xs ]

-- sumpair (x,y) = x+y
-- op a b = map sumpair a ++ b

-- myconcat = foldl (op) []

-- fibonnaci
add [] [] = []
add (a:as) (b:bs) = (a+b) : (add as bs)

fibs = 1 : 1 : add fibs (tail fibs)

splits []         = [([],[])]
splits (x:xs)     = ([],x:xs) : [(x:ps,qs) | (ps,qs)<-splits xs]

permSplit []         = [[]]
permSplit (x:xs)     = [ ps ++ [x] ++ qs | rs <- permSplit xs,
                                            (ps,qs)<- splits rs]