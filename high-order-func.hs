import Data.List
-- 1
length x = sum (map (const 1) x)

-- 2
a x = map (+1) (map (+1) x)
b x f g = map (f.g) x

-- 3

-- iter :: Integer -> (t -> t) -> t -> t
-- iter times func val
--     | times < 0     = error "wrong time data given"
--     | times == 0    = val
--     | otherwise     = iter (times - 1) func (func val)
iter n func val
    | n < 0 = error "wrong time data given"
    | n == 0 = val
    | otherwise = iter (n -1) func (func val)
    -- -- or
-- iter 0 func val = val
-- iter n func val = iter (n-1) func (func val)
no4 :: Integer -> Integer -> Integer
no4 = \n -> iter n succ
-- 5

sumOfSquareWithMap :: Integer -> Integer
sumOfSquareWithMap n = sum (map (\x -> x*x ) [1..n])

sumOfSquareWithFoldr :: [Integer] -> Integer
sumOfSquareWithFoldr = foldr ((+).(^2)) 0

toNSquareSum :: Integer -> Integer
toNSquareSum val
    | val > 0      = foldr (+) 0 (map (\x -> x * x) [1 .. val])
    | otherwise    = error "scope error"

listSquareSum :: [Integer] -> Integer
listSquareSum ls    = foldr (+) 0 (map (\x -> x * x) ls)


-- 6 --> add xs with empty array, basically it does nothing
mystery xs = foldr (++) [] (map sing xs)
  where
    sing x = [x]

-- 7
-- compose :: [a -> a] -> a -> a
-- compose fs v = foldl (flip (.)) id fs $ v

-- -- 8
customflip :: (a -> b -> c ) -> b -> a -> c
-- flip \ 4 5 -> 5 / 4
customflip f = g
    where
      g x y = f y x

---- LIST COMPREHENSION
l1 = [ x+y | x<- [1,2], y<-[3,4]]
l3 = [ x+2 | x<- [1,2,3,4], x>3]
customl3 x= map (+2) (filter (>3) x)
l4 = [ x+3 | (x,_) <- [(1,2),(3,4)]]

-- 1. [ x+1 | x <- xs ]
-- plusOne = map (+1)
-- 2. [ x+y | x <- xs, y <-ys ]
-- func1 x y = zipWith (+) x y
-- plusXY = map
-- 3. [ x+2 | x <- xs, x > 3 ]

-- 4. [ x+3 | (x,_) <- xys ]
-- 5. [ x+4 | (x,y) <- xys, x+y < 5 ]
-- 6. [ x+5 | Just x <- mxs ]

-- maximum (x:xs)
--     | (maximum xs) > x = maximum xs
--     | otherwise        = x

-- permutation
-- perm [] = perm [[]]
-- perm ls = [ x:ps | x <- ls, ps <- perm(ls\\[x])]

-- sumList :: [Int] -> Int
sumList [] = 0
sumList (x:xs) = x + sumList xs

maxTiga a b c = max a (max b c)

quickSort [] = []
quickSort (x:xs) = quickSort [y | y <- xs, y <= x] ++ x ++ quickSort [y | y <- xs, y > x]



perm [] = [[]]
perm l = [x:y | x <- l, y <- perm (l \\ [x])]

primes = sieve [2..]
  where sieve (x:xs) = x : sieve [y | y<- xs, y `mod` x /= 0]

pythaTriple = [(x,y,z) | z <- [5..]
                        ,y <- [z, z-1 .. 1]
                        ,x <- [y,y-1 .. 1]
                        , z*z == x*x + y*y]


pythaTriple2 = [(x,y,z) | z <- [5..],
                          y <- [1..z],
                          x <- [1..y],
                          z*z == x*x + y*y]
pythaTriple3 = [(x,y,z) | y <- [1..],
                          x <- [1..y],
                          z <- [5..],
                          z*z == x*x + y*y]

-- concat [] = []
-- concat (xs: xss) = xs ++ concat xss

-- misteri xs ys = concat [ map (\x -> map(\y -> (x,y)) ys) xs ]

-- sumpair (x,y) = x+y
-- op a b = map sumpair a ++ b

-- myconcat = foldl (op) []

data Move = Paper | Rock | Scissors
  deriving Show

moves :: Integer -> Move
moves 1 = Paper
moves 2 = Rock

data Tagger = Tagn Integer | Tagb Bool

data Shape = Rectangle Float Float
            | Ellipse Float Float
            | Polygon [(Float,Float)]
          deriving Show
area :: Shape -> Float
area (Rectangle s1 s2) = s1*s2
area (Ellipse r1 r2) = pi * r1 * r2
-- area (Polygon v1:pts) = polyArea pts
--   where polyArea:: [(Float,Float) ->  Float]
--         polyArea (v2:v3:vs) = triArea

ax [] = []
ax (c:cs) = 1 : ax cs

mergeList :: [[Integer]] -> [Integer]
mergeList = foldl (++) []

reversed xs = foldl revOp [] xs
            where revOp a b = b : a

reversed2 :: [a] -> [a]
reversed2 = foldl (flip (:)) []
